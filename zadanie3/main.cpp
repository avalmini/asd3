#include <iostream>
#include <fstream>

using namespace std;

void load(int** &tab,int** tmp,int &n,int &m,int &p){
    ifstream wejscie;
    ofstream wyjscie;
    wejscie.open("dane.txt");
    wyjscie.open("out.txt");
    wejscie>>n;
    tab = new int*[n];
    for(int i = 0; i < n; i++){
        tab[i] = new int[3];
    }

    for(int i=0;i<n;i++){
        for(int j=0;j<3;j++){
            wejscie>>tab[i][j];
        }
        if(tab[i][1]>m)m=tab[i][1];//najpóźniej kończąca się rezerwacja
        if(tab[i][0]>p)p=tab[i][0];//najpóźniej zaczynająca się rezerwacja
    }


}

int partition(int** &tab, int p, int r){
    //int x = tab[p];
    int x[3];
    for(int i=0;i<3;i++)x[i]=tab[p][i];
    int i = p, j = r, w;
    while (true){
        while (tab[j][1] > x[1]){
            j--;
        }
        while (tab[i][1] < x[1]){
            i++;
        }
        if (i < j){
            for(int a=0;a<3;a++){w = tab[i][a];
            tab[i][a] = tab[j][a];
            tab[j][a] = w;
            }
            i++;
            j--;

        }
        else
            return j;
    }
}

void quicksort(int** &tab, int p, int r){
    int q;
    if (p < r){
        q = partition(tab,p,r);
        quicksort(tab, p, q);
        quicksort(tab, q+1, r);
    }
}

int alg(int** tab, int** &tmp, int n, int m){
    tmp = new int*[m];
    for(int i = 0; i < m; i++){
        tmp[i] = new int[m];
    }

    for(int i = 0; i < m; i++){
        for(int j=0;j<m;j++)
            tmp[i][j]=0;
    }

    int j=0;

    for(int a=2;a<=m;a++){
        if(a==tab[j][1]){
            for(int i=0;i<a-1;i++){
                tmp[i][a-1]=max(tab[j][2]+tmp[i][tab[j][0]-1],tmp[i][a-2]);
            }

            for(int i=0;i<a-1;i++){
                tmp[a-1][i]=max(tab[j][2]+tmp[tab[j][0]-1][i],tmp[a-2][i]);
            }
            j++;
        }
        else{
            for(int i=0;i<a-1;i++){
                tmp[i][a-1]=tmp[i][a-2];
                tmp[a-1][i]=tmp[a-2][i];
            }
        }
        tmp[a-1][a-1]=max(tmp[a-1][a-2],tmp[a-2][a-1]);
    }

    return tmp[m-1][m-1];
}

void output(int** &tab,int n){
    for(int i=0;i<n;i++){
        for(int j=0;j<3;j++)
            cout<<tab[i][j]<<" ";
    cout<<endl;
    }
}

int main()
{
    int n,m=0,p=0;
    int** tab;
    int** tmp;
    load(tab,tmp,n,m,p);
    quicksort(tab,0,n-1);
    cout<<alg(tab,tmp,n,m);
//    output(tab,n);
    return 0;
}
